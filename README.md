IR Learn Laravel Authentication
-------------------------------

this is a simple authentication web powered by laravel 5.4.28

####-Initiating Project-
You can create a laravel project with two ways,

- ```$ composer create-project laravel/laravel PROJECTNAME```.
or
- ```$ laravel new PROJECTNAME```.

  >**Note:**
  >but don't forget to turn on laravel mode via composer $ composer global require "laravel/installer"

- link storage ```$ php artisan storage:link``s  

- use image intervantion ```$composer require intervention/image```

- custom styles ```$ npm install```

- compile sass ```$ npm run development```


####-Laravel Comands you need-  
[x] turn on Authentication component Laravel
    ```$ php artisan make:auth```

[x] migrate auth database
    ```$ php artisan migrate```

[x] create a controller class
    ```$ php artisan make:controller UserController --resource```

#####-Problem Founded while developing-##
- (SQLSTATE[42000]: syntax error or access violation: 1071) https://laravel-news.com/laravel-5-4-key-too-long-error

####-Installation project on your localhost
- ```$ composer install ```

- ```$ php artisan key:generate ```

- setup your database with modify ```.env```

